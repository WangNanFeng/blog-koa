const redis = require("redis")
const { REDIS_CONF } = require("../conf/db")

// 创建客户端
const redisClient = redis.createClient(REDIS_CONF.port, REDIS_CONF.host)

!(async function () {
  // 链接
  await redisClient
    .connect()
    .then(() => console.log("Redis connect success"))
    .catch((err) => console.error(err))
})()

async function set(key, val) {
  if (typeof val === "object") {
    val = JSON.stringify(val)
  }

  await redisClient.set(key, val)
}

async function get(key) {
  try {
    const val = await redisClient.get(key)
    if (val == null) {
      return null
    }
    try {
      return JSON.parse(val)
    } catch (err) {
      return val
    }
  } catch (err) {
    throw err
  }
}

module.exports = {
  set,
  get,
}
