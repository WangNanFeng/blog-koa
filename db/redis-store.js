const redisClient = require("./redis")
module.exports = {
  get(key, maxAge, { rolling, ctx }) {
    return redisClient.get(key)
  },
  set(key, sess, maxAge, { rolling, changed, ctx }) {
    redisClient.set(key, sess)
  },
  destroy(key, { ctx }) {
    redisClient.del(key)
  },
}
