const createError = require("http-errors")
const Koa = require("koa")
const bodyParser = require("koa-bodyparser")
const cookieParser = require("cookie-parser")
const logger = require("morgan")
const session = require("koa-session")
const redisStore = require("./db/redis-store")
// const RedisStore = require("connect-redis")(session)

// const redisClient = require("./db/redis")
// const sessionStore = new RedisStore({ client: redisClient })

const blogRouter = require("./routes/blog")
const userRouter = require("./routes/user")
// const router = require("./routes/index")

const app = new Koa()

app.keys = ["WangNanFeng"]

app.use(bodyParser())
// app.use(logger("dev"))

// app.use(cookieParser())
app.use(
  session(
    {
      key: "Authorization" /** (string) cookie key (default is koa.sess) */,
      /** (number || 'session') maxAge in ms (default is 1 days) */
      /** 'session' will result in a cookie that expires when session/browser is closed */
      /** Warning: If a session cookie is stolen, this cookie will never expire */
      maxAge: 24 * 60 * 60 * 1000,
      autoCommit: true /** (boolean) automatically commit headers (default true) */,
      overwrite: true /** (boolean) can overwrite or not (default true) */,
      httpOnly: true /** (boolean) httpOnly or not (default true) */,
      signed: true /** (boolean) signed or not (default true) */,
      rolling: false /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */,
      renew: false /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/,
      secure: false /** (boolean) secure cookie*/,
      sameSite: null,
      store: redisStore,
    },
    app
  )
)

app.use(userRouter.routes()).use(userRouter.allowedMethods())
app.use(blogRouter.routes()).use(blogRouter.allowedMethods())

// app.use("/api/blog", blogRouter)
// app.use("/api/user", userRouter)

// catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   next(createError(404))
// })

// // error handler
// app.use(function (err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message
//   res.locals.error = req.app.get("env") === "development" ? err : {}

//   // render the error page
//   res.status(err.status || 500)
//   res.render("error")
// })

// logger

// app.use(async (ctx, next) => {
//   await next()
//   const rt = ctx.response.get("X-Response-Time")
//   console.log(`${ctx.method} ${ctx.url} - ${rt}`)
// })

// // x-response-time

// app.use(async (ctx, next) => {
//   const start = Date.now()
//   await next()
//   const ms = Date.now() - start
//   ctx.set("X-Response-Time", `${ms}ms`)
// })

// // response

// app.use(async (ctx) => {
//   ctx.body = "Hello World"
// })

module.exports = app
