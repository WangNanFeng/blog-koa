const Router = require("@koa/router")
const blogRouter = require("./blog")

const router = new Router()

router.use("/api/blog", blogRouter.routes, blogRouter.allowedMethods())

module.exports = router
