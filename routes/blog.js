const Router = require('@koa/router')
const {
  getList,
  getDetail,
  newBlog,
  updateBlog,
  delBlog,
} = require('../controller/blog')
const { get } = require('../db/redis')
const { SuccessModel, ErrorModel } = require('../model/resModel')

// 统一的登录验证函数
const loginCheck = (ctx) => {
  const Authorization = ctx.cookies.get('Authorization')
  const { username } = get(Authorization)
  if (!username) {
    ctx.body = new ErrorModel('尚未登录')
  }
}

const router = new Router({
  prefix: '/api/blog',
})

router.get('/list', async (ctx) => {
  const { author, keyword } = ctx.query
  const result = await getList(author, keyword)
  ctx.body = new SuccessModel(result)
})

router.get('/detail', async (ctx) => {
  const id = ctx.query.id
  const result = await getDetail(id)
  ctx.body = new SuccessModel(result)
})

router.post('/new', async (ctx) => {
  loginCheck(ctx)
  const blogData = ctx.request.body
  const result = await newBlog(blogData)
  ctx.body = new SuccessModel(result)
})

router.post('/update', async (ctx) => {
  loginCheck(ctx)
  const id = ctx.query.id
  const blogData = ctx.request.body
  const result = await updateBlog(id, blogData)
  ctx.body = new SuccessModel(result)
})

router.post('/del', async (ctx) => {
  loginCheck(ctx)
  const id = ctx.query.id
  const author = ctx.session.username
  const result = await delBlog(id, author)

  if (result) {
    ctx.body = new SuccessModel()
  } else {
    ctx.body = new ErrorModel()
  }
})

module.exports = router
