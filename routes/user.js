const Router = require("@koa/router")
const { login } = require("../controller/user")
const { SuccessModel, ErrorModel } = require("../model/resModel")

const router = new Router({ prefix: "/api/user" })
router.post("/login", async (ctx) => {
  const { username, password } = ctx.request.body
  const result = await login(username, password)
  if (result.username) {
    // 操作cookie
    ctx.session.username = result.username
    ctx.session.realname = result.realname
    ctx.body = new SuccessModel()
    return
  }
})

// router.get("/login-test", (req, res, next) => {
//   if (req.session.username) {
//     res.json({
//       errno: 0,
//       msg: "已登录",
//     })
//     return
//   }
//   res.json({
//     errno: -1,
//     msg: "未登录",
//   })
// })

// router.get("/session-test", (req, res, next) => {
//   const session = req.session
//   if (session.viewNum == null) {
//     session.viewNum = 0
//   }
//   session.viewNum++
//   res.json({
//     viewNum: session.viewNum,
//   })
// })

module.exports = router
